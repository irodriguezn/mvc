/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.Alumno;
import modelo.ModeloAlmacen;
import vista.Vista;
import vista.VistaAlumno;

/**
 *
 * @author lliurex
 */
public class Controlador {
    
    Alumno al;
    ModeloAlmacen alumnos;
    Vista vista;
    VistaAlumno vAlumno;    
    
    public Controlador(Vista v, ModeloAlmacen m) {
        this.vista=v;
        this.alumnos=m;
        VistaAlumno va=new VistaAlumno();
        for (int i=1; i<=3; i++) {
            al=va.leerAlumno(alumnos);
            alumnos.addAlumno(al);
        }
        alumnos.borrarAlumno("manolo");
        alumnos.mostrarAlumnos();
        
    }
    
}
