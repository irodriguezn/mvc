/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author lliurex
 */
public class ModeloAlmacen {
    private LinkedList<Alumno> alumnos=new LinkedList<>();
    
    public void addAlumno(Alumno al) {
        alumnos.add(al);
    }
    
    public boolean comprobarNombre(String nombre) {
        boolean encontrado=false;
        Alumno al;
        Iterator it=alumnos.iterator();
        while (it.hasNext()) {
            al=(Alumno)it.next();
            if (al.getNombre().equalsIgnoreCase(nombre)){
                encontrado=true;
            }
        }
        return encontrado;
    }
    
    public void mostrarAlumnos() {
        System.out.println(alumnos);
    }
    
    public void borrarAlumno(String nombre) {
        Iterator it;
        Alumno al;
        boolean salir=false;
        while (!salir) {
            salir=true;
            it=alumnos.iterator();
            while (it.hasNext()) {
                al=(Alumno)it.next();
                if (al.getNombre().equalsIgnoreCase(nombre)) {
                    alumnos.remove(al);
                    salir=false;
                    break;
                }
            }
        }
    }
    
}
