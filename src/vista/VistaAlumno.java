/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Scanner;
import modelo.Alumno;
import modelo.ModeloAlmacen;

/**
 *
 * @author lliurex
 */
public class VistaAlumno {
    public Alumno leerAlumno(ModeloAlmacen ma) {
        String nombre;
        int edad;
        Scanner sc=new Scanner(System.in);
        System.out.print("Introduce nombre: ");
        nombre=sc.nextLine();
        while(ma.comprobarNombre(nombre)) {
            System.out.println("El nombre ya existe!!");
            System.out.print("Introduce nombre: ");
            nombre=sc.nextLine();
        }
        System.out.print("Introduce edad: ");
        edad=sc.nextInt();
        Alumno al=new Alumno(nombre, edad);
        return al;
    }
    
    public void mostrarAlumno(Alumno al) {
        System.out.println(al.toString());
    }
}
